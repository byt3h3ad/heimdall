const express = require('express')
const app = express()
// const port = 3000

app.get('/', (req, res) => {
    const ip = req.header('x-forwarded-for') || req.socket.remoteAddress;
    res.send(`Hello World! ${ip}`)
})

// app.listen(port, () => {
//     console.log(`Example app listening on port ${port}`)
// })

module.exports = app;
